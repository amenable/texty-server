
var spawn = require('child_process').spawn;
var wrapProcess = require('./wrapProcess');
var parseFunction = function(output){
    return output;
};
function NodeSentiment(){
    this.type = 'node'
    this.process = spawn('node', ['./lib/sentiment-scripts/node-sentiment.js']);
    this.process.stdin.setEncoding('utf-8');
    this.process.stdout.setEncoding('utf-8');
    this.analyse = wrapProcess(this.process, parseFunction);
}
module.exports = NodeSentiment;
