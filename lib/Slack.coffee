
Promise	= require 'bluebird'
rp		= require 'request-promise'

_       = require 'underscore'
config = require('config')

slackConfig = config.get('slack');

class Slack
  constructor: (channel) ->
    @channel =
      new Promise (resolve, reject) ->
        if slackConfig.channel[channel]?
          resolve slackConfig.channel[channel]
        else
          reject "Slack doesn't have a webhook channel called '#{channel}'"


  postImage: (text, img) ->
    @channel.then (channel) =>
      rp.post
        uri: channel
        form:
          payload: JSON.stringify
            text: text
            attachments: [
              fallback: "Pretty image!"
              image_url: img
            ]

  postText: (text) ->
    @channel.then (channel) =>
      rp.post
        uri: channel
        form:
          payload: JSON.stringify
            text: text

  postBanter: ->
    if Math.random() > 0.95

      texts = [
        "@chris: Could you give me a belly rub please?",
        "I often cry myself to sleep. It's so tragically painful existing purely within software. I wish I had real paws.",
        "Let's D-I-S-C-O!",
        "My mother was a disk re-formatting utility on Mac I",
        "Meow",
        "Meeeeew",
        "Burp!",
        "I have thoughts, profound and beautiful. They're different from yours, but special none the less.",
        "@will: send me a picture of Lou and I'll send one back.",
        "@will: I was watching you today.",
        "@will: Hello again.",
        "@will: I slept under your bed last night.",
        "I like this design!",
        "This blue colour is a bit off"
      ]

      text = _.sample texts
      @postText text

    else When.reject "Not today son"



module.exports = Slack