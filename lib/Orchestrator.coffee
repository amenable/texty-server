SlackApp = require './SlackApp.coffee'
Promise = require 'bluebird'
_ = require 'underscore'
ChatBot = require './ChatBot'

ResponsePicker = require './ResponsePicker'

NodeSentiment = require './NodeSentiment'
RubySentiment = require './RubySentiment'
PythonSentiment = require './PythonSentiment'

BotSocket = require './BotSocket'


module.exports = () ->

  responsePicker = new ResponsePicker()

  sentiments = [
    new NodeSentiment(),
    new RubySentiment(),
    new PythonSentiment()
  ]



  slack = new SlackApp()
  bot = new ChatBot()

  onMessage = (data) ->
    channel = data.channel
    sample = data.text
    console.log data
    username = slack.getUserRealName(data.user) || 'Dave'
    tokens = sample.trim().split(' ')
    console.log 'tokens', tokens
    filtered = _.filter(tokens, (w) ->
      return w.indexOf('@') == -1 && w.length > '2'
    )
    console.log 'filtered', filtered

    if filtered.length == 0
      message = "I'm not sure what you want to talk about. Can you summarize in a single word?"
      slack.postMessage message, channel
    else if filtered.length > 1
      message = "Whoah! Too much information. Can you summarize what you want to talk about in a single word?"
      slack.postMessage message, channel
    else
      word = filtered[0]
      console.log 'say to bot:', word
      botSays = bot.sayToBot(word)
      botSays.then (botSays) ->
        botSays = responsePicker.pick botSays, username
        console.log 'channel', channel
        console.log 'botSays', botSays
        slack.postMessage botSays, channel




  process.stdin.setEncoding 'utf-8'
  process.stdin.on "data", (data) ->
    try
      botSays = bot.sayToBot(data)
      botSays.then (botSays) ->
        botSays = responsePicker.pick botSays
        slack.postMessage(botSays)

    catch e
      console.error "error!",
        input: data
        err: e

  try
    botSocket = new BotSocket(onMessage)
  catch e
    console.error "unable to initialise bot", e




  #slack = new SlackApp().postMessage(botSays)
###
  slack.api "users.list", (err, response) ->
  if err
    console.error err
  console.log response
###
