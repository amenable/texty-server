var spawn = require('child_process').spawn;
var wrapProcess = require('./wrapProcess');
var parseFunction = function(output){
    return output;
};
function RubySentiment(){
    this.type = 'ruby'
    this.process = spawn('ruby', ['./lib/sentiment-scripts/rb-sentimental.rb']);
    this.process.stdin.setEncoding('utf-8');
    this.process.stdout.setEncoding('utf-8');
    this.analyse = wrapProcess(this.process, parseFunction);
}
module.exports = RubySentiment;
