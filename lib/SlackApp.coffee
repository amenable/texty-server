
config = require 'config'
Promise = require 'bluebird'
Slack = require 'slack-node'
_ = require 'underscore'
fs = require 'fs'


slackConfig = config.get 'slack'
dataConfig = config.get 'data'

demoChannel = "#texty-demo"

class SlackApp
  constructor: () ->
    apiToken = slackConfig.apiKey
    @slackApi = new Slack(apiToken)
    @channelCache = {}
    @userCache = {}
    @reverseChannelCache = {}
    Promise.promisifyAll(@slackApi)
    @getChannelList()
    setInterval () =>
      @getChannelList()
    ,5000
    setInterval () =>
      @getUserList()
    ,30000

  getChannelList: () ->
    @slackApi.apiAsync("channels.list").then (response) =>
      console.log "fetching channel list"
      _.reduce response.channels, (memo, entry) ->
        memo[entry.name] = entry.id
        memo["##{entry.name}"] = entry.id
        memo
      , @channelCache
      _.extend @reverseChannelCache, _.invert(@channelCache)
      @getUserList()

  getUserList: () ->
    @slackApi.apiAsync("users.list").then (response) =>
      console.log "fetching user list"
      _.reduce response.members, (memo, entry) ->
        memo[entry.id] = entry.real_name
        memo["@#{entry.id}"] = entry.real_name
        memo
      , @userCache

  getUserRealName: (id) ->
    #console.log id, @userCache
    @userCache[id]

  pollForReactions: (interactionRecord, timestamp) ->
    startPollingTime = new Date()
    interval = setInterval () =>
      timeElapsed = new Date() - startPollingTime

      if timeElapsed > 1000 * 60 * 30
        clearInterval interval

      log = _.once (input) ->
        console.log input

      reaction = @slackApi.apiAsync 'reactions.get',
        channel: @channelCache[demoChannel]
        timestamp: timestamp
        full: true
      reaction.then (resp) ->

        #console.log 'reactions', resp
        if resp.message?.reactions?

          reactions = resp.message.reactions

          log reactions
          #console.log reactions
          reactions = _.map reactions, _.partial( _.pick, _, 'name', 'count')
          interactionRecord.reactions = reactions

        #write file with user-supplied keyword, our response, list of reactions
        fs.writeFile "#{dataConfig.outputDir}#{timestamp}.txt", JSON.stringify(interactionRecord), (err) ->
          if err
            console.log err

    , 5000

  postMessage: (botSays, channel) ->
    channel = channel || demoChannel
    if !@channelCache[channel]
      console.log 'cannot find channel', channel
      console.log 'cache', @reverseChannelCache
      channel = @reverseChannelCache[channel]
      console.log 'channel', channel
    post = @slackApi.apiAsync 'chat.postMessage',
      text:botSays,
      channel: channel

    post.catch (err) ->
      console.error err, arguments

    timestamp = post.then (resp) ->
      # console.log(resp)
      resp.ts

    interactionRecord =
      userSaid : 'cheese'
      botSays: botSays

    poll = (timestamp) =>
      @pollForReactions(interactionRecord, timestamp)


    timestamp.then poll, 5000


module.exports = SlackApp










