_ = require 'underscore'

randomChoice = (arr) ->
  randIndex = Math.floor(Math.random() * arr.length - 0.00001)
  arr[randIndex]

class ResponsePicker
    constructor: () ->

    pick: (data, username) ->
      console.log 'username', username
      data = if _.isArray(data) then randomChoice data else data
      data = data.replace new RegExp('<person>', 'g'), username
      data = data.replace new RegExp('<unk>', 'g'), username
      data = data.replace new RegExp(" '", 'g'), "'"
      data = data.replace new RegExp("<s>", 'g'), ""
      data = data.replace new RegExp("</s>", 'g'), ""
      data = data.replace new RegExp("   ", 'g'), " "
      data = data.replace new RegExp("  ", 'g'), " "
      data

module.exports = ResponsePicker