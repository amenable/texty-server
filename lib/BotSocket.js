var slackAPI = require("slackbotapi");
var token = require("../config/default.json").slack.botToken;
var _ = require("underscore");

var acceptMsg = function(data){
  return typeof data.text !== 'undefined'
      && !["bot", "texty-bot"].some(function(el){
        return data.username === el;
      })
      && ["@bot", "<@U0KSCE0PQ>"].some(function(el){
        return data.text.indexOf(el) !== -1;
      });
};

var BotSocket = function(cb){
  this.send = function (channel, message){
    bot.sendMsg(data.channel, data.text)
  };

  var bot = new slackAPI({
    "token": token,
    "logging": true,
    "autoReconnect": true
  });
  bot.on("message", function(data){
    try {
      console.log(data.text);
      if (!acceptMsg(data)) return;
      //bot.sendMsg(data.channel, data.text);
      console.log("data:", data);
      if (cb) cb(data);

      console.log("\n@\n");
    }
    catch(err) {
      console.error("error occured", {err: err, input: data});
      //console.log(data.text);
    }
  });
  bot.on("reaction_added", function(data){
    console.log("reaction added");
  });
  bot.on("presence_change", function(data){
    console.log("presence change");
  });
};

module.exports = BotSocket;
