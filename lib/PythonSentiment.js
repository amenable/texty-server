/**
 *  * Created by andrew on 30/01/2016.
 *   */

var spawn = require('child_process').spawn;
var wrapProcess = require('./wrapProcess');
var parseFunction = function(output){
    return output;
};
function PythonSentiment(){
    this.type = 'python'
    this.process = spawn('python', ['./lib/sentiment-scripts/py-textblob.py']);
    this.process.stdin.setEncoding('utf-8');
    this.process.stdout.setEncoding('utf-8');
    this.analyse = wrapProcess(this.process, parseFunction);
}
module.exports = PythonSentiment;
