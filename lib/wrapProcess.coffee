guard = require 'when/guard'
When = require 'when'
_ = require 'underscore'

###
  Example usage

  proc = spawn('sh', ['redirect.sh']);
  proc.stdin.setEncoding('utf-8');
  proc.stdout.setEncoding('utf-8');

  communicateWithMyProc = wrapProcess proc, _.identity

  response = communicateWithMyProc("Anime is my favorite thing to watch")
  response.then (resp) ->
    console.log "Sentiment was" + resp.sentiment
###

wrapProcess = (spawnedProc, outFunc) ->
  spawnedProc.on('close', (code) =>
    console.error("child process exited with code #{code}")
  );

  functionWrapper = (spawnedProc, outFunc, input) ->
    #console.log 'calling node', input
    When.promise (resolve, reject) ->
      onData = (response) ->
        #console.log 'recieved response', response
        spawnedProc.stderr.removeListener 'error', onError
        resolve outFunc(response)


      onError = (err) ->
        spawnedProc.stderr.removeListener 'error', onError
        reject err
      spawnedProc.stdout.once 'data', onData
      spawnedProc.stderr.on 'error', onError
      spawnedProc.stdin.write(input)
      spawnedProc.stdin.write('\n')

  commWithMyProc = _.partial functionWrapper, spawnedProc, outFunc
  guardedAsyncOperation = guard(guard.n(1), commWithMyProc)
  guardedAsyncOperation

module.exports = wrapProcess

