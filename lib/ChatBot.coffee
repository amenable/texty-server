spawn = require('child_process').spawn;
wrapProcess = require './wrapProcess'
_ = require 'underscore'

config = require 'config'

nlpConfig = config.get('nlp')

path = nlpConfig.path[0]
processToRun = nlpConfig.path[1]
processArguments = nlpConfig.path.slice(2)

class ChatBot
  constructor: () ->
    console.log 'args', processArguments
    @proc = spawn(processToRun, processArguments,
      cwd: path
    )
    @proc.stdin.setEncoding('utf-8');
    @proc.stdout.setEncoding('utf-8');
    @proc.stdout.on "data", (data) ->
      console.log "chatbot:", data
    @proc.stderr.on "error", (data) ->
      console.log "chatbot:", data
    @sayToBot = wrapProcess @proc, (response) =>
        @parseResponse response

  parseResponse: (response) ->
    response = response.replace('[]', '')
    console.log 'response', response

    startAt = response.indexOf('[')
    endAt = response.lastIndexOf(']')
    strToParse = response.substr(startAt)
    cutOffEnd = response.length-endAt-1

    lengthToParse = response.length-startAt-cutOffEnd
    console.log 'parsefor', lengthToParse
    console.log 'substr', response.substr(startAt, response.length-startAt-cutOffEnd)
    botText = if startAt == -1 then response else _.flatten JSON.parse(response.substr(startAt, response.length-startAt-cutOffEnd))
    console.log 'bot suggests', botText
    botText




class DemoChatBot
  constructor: () ->

    @proc = spawn('sh', ['redirect.sh'])
    @proc.stdin.setEncoding('utf-8')
    @proc.stdout.setEncoding('utf-8')

    @sayToBot = wrapProcess @proc, _.identity


module.exports = ChatBot


